package com.hackerearth.chess.service;

import com.hackerearth.chess.entity.ChessGame;
import com.hackerearth.chess.web.MoveRequest;

public interface MovesService {

	String movePiece(MoveRequest moveRequest);
	
	ChessGame getGame(String token);

}
