package com.hackerearth.chess.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackerearth.chess.constant.PieceType;
import com.hackerearth.chess.constant.Player;
import com.hackerearth.chess.entity.ChessGame;
import com.hackerearth.chess.entity.ChessGame.Move;
import com.hackerearth.chess.entity.ChessGame.Piece;
import com.hackerearth.chess.entity.ChessGame.Position;
import com.hackerearth.chess.repository.ChessGameRepository;
import com.hackerearth.chess.service.MovesService;
import com.hackerearth.chess.web.MoveRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MovesServiceImpl implements MovesService {

	private static final String INVALID = "Invalid";
	private static final String VALID = "Valid";

	@Autowired
	private ChessGameRepository repository;
	
	@Override
	public String movePiece(MoveRequest request) {
		if (request.getNotation().equals("START")) {
			ChessGame game = new ChessGame();
			game = repository.save(game);
			return game.getToken();
		}
		if (StringUtils.isBlank(request.getNotation())) {
			return INVALID;
		}
		ChessGame game = repository.findById(request.getToken()).orElse(null);
		if (Objects.isNull(game) || Objects.nonNull(game.getWinner())) {
			return INVALID;
		}
		int x = 7;
		Player player = Player.BLACK;
		if (game.isWhiteToMove()) {
			player = Player.WHITE;
			x = 0;
		}
		// check if castling
		if (request.getNotation().equals("O-O")) {
			// king side castle
			if (!isKingUntouched(player, game.getMoves())) {
				return INVALID;
			}
			if (!checkIfKingSideCastlingPossible(x, game.getBoard())) {
				return INVALID;
			}
			// perform castling
			performKingSideCastling(x, game, player);
			return VALID;
		} else if (request.getNotation().equals("O-O-O")) {
			// queen side castle
			if (!isKingUntouched(player, game.getMoves())) {
				return INVALID;
			}
			if (!checkIfQueenSideCastlingPossible(x, game.getBoard())) {
				return INVALID;
			}
			// perform castling
			performQueenSideCastling(x, game, player);
			return VALID;
		}

		Move move = getMove(player, request.getNotation());
		log.info("move {}", move);
		if (Objects.isNull(move)) {
			return INVALID;
		}
		Piece opponentPiece = game.getBoard()[move.getDestination().getX()][move.getDestination().getY()];
		// if the notation says no takes, there should be no piece at destination
		// position
		if (!move.isTakes() && Objects.nonNull(opponentPiece)) {
			return INVALID;
		}
		// if the notation says takes, there should only be an opposite color piece
		// at destination position
		else if (move.isTakes() && (Objects.isNull(opponentPiece) || player.equals(opponentPiece.getPlayer()))) {
			return INVALID;
		}
		Position currentPosition = isMoveValid(move, game.getBoard());
		if (Objects.isNull(currentPosition)) {
			return INVALID;
		}

		// check if there is KING at destination to check if win
		if (Objects.nonNull(opponentPiece) && opponentPiece.getPieceType().equals(PieceType.KING)) {
			if (move.getPieceToMove().getPlayer().equals(Player.WHITE)) {
				performMove(game, move, currentPosition, Player.WHITE);
				return "White Wins";
			} else {
				performMove(game, move, currentPosition, Player.BLACK);
				return "Black Wins";
			}
		}
		performMove(game, move, currentPosition, null);
		return VALID;
	}

	private boolean isKingUntouched(Player player, List<Move> moves) {
		for (Move move : moves) {
			if (move.getPieceToMove().getPlayer().equals(player)
					&& move.getPieceToMove().getPieceType().equals(PieceType.KING)) {
				return false;
			}
		}
		return true;
	}

	private boolean checkIfKingSideCastlingPossible(int x, Piece[][] board) {
		Piece king = board[x][4];
		Piece rook = board[x][7];
		// king and rook should be in their proper places
		if (Objects.isNull(king) || !king.getPieceType().equals(PieceType.KING) || Objects.isNull(rook)
				|| !rook.getPieceType().equals(PieceType.ROOK)) {
			return false;
		}
		// there should be no piece in between king and rook
		if (Objects.nonNull(board[x][5]) || Objects.nonNull(board[x][6])) {
			return false;
		}
		// ignoring the condition that king should not be in check
		return true;
	}

	private boolean checkIfQueenSideCastlingPossible(int x, Piece[][] board) {
		Piece king = board[x][4];
		Piece rook = board[x][0];
		// king and rook should be in their proper places
		if (Objects.isNull(king) || !king.getPieceType().equals(PieceType.KING) || Objects.isNull(rook)
				|| !rook.getPieceType().equals(PieceType.ROOK)) {
			return false;
		}
		// there should be no piece in between king and rook
		if (Objects.nonNull(board[x][1]) || Objects.nonNull(board[x][2]) || Objects.nonNull(board[x][3])) {
			return false;
		}
		// ignoring the condition that king should not be in check
		return true;
	}

	private void performKingSideCastling(int x, ChessGame game, Player player) {
		Piece[][] board = game.getBoard();
		board[x][4] = null;
		board[x][7] = null;
		board[x][6] = new Piece(player, PieceType.KING);
		board[x][5] = new Piece(player, PieceType.ROOK);
		game.setBoard(board);
		game.setWhiteToMove(!game.isWhiteToMove());
		game.getMoves().add(new Move("KING", player));
		repository.save(game);
	}

	private void performQueenSideCastling(int x, ChessGame game, Player player) {
		Piece[][] board = game.getBoard();
		board[x][4] = null;
		board[x][0] = null;
		board[x][2] = new Piece(player, PieceType.KING);
		board[x][3] = new Piece(player, PieceType.ROOK);
		game.setBoard(board);
		game.setWhiteToMove(!game.isWhiteToMove());
		game.getMoves().add(new Move("QUEEN", player));
		repository.save(game);
	}

	private void performMove(ChessGame game, Move move, Position currentPosition, Player winner) {
		Piece[][] board = game.getBoard();
		board[currentPosition.getX()][currentPosition.getY()] = null;
		board[move.getDestination().getX()][move.getDestination().getY()] = move.getPieceToMove();
		game.setBoard(board);
		game.setWhiteToMove(!game.isWhiteToMove());
		game.getMoves().add(move);
		if (Objects.nonNull(winner)) {
			game.setWinner(winner);
		}
		repository.save(game);
	}

	/**
	 * Breaks the notation up into understandable format, which player is doing the
	 * move, what piece is being moved, is this move capturing any piece and what is
	 * the destination coordinates of the move.
	 * 
	 * @param player
	 *            who is doing the move
	 * @param notation
	 *            given notation
	 * @return move to be played
	 */
	private Move getMove(Player player, String notation) {
		String regExp = "([KQBNR]?|[a-h])x?[a-h][1-8]";
		if (!notation.matches(regExp)) {
			return null;
		}
		Move move = new Move();
		char p = notation.charAt(0);
		switch (p) {
		case 'K':
			move.setPieceToMove(new Piece(player, PieceType.KING));
			break;
		case 'Q':
			move.setPieceToMove(new Piece(player, PieceType.QUEEN));
			break;
		case 'B':
			move.setPieceToMove(new Piece(player, PieceType.BISHOP));
			break;
		case 'N':
			move.setPieceToMove(new Piece(player, PieceType.KNIGHT));
			break;
		case 'R':
			move.setPieceToMove(new Piece(player, PieceType.ROOK));
			break;
		default:
			move.setPieceToMove(new Piece(player, PieceType.PAWN));
		}
		if (notation.contains("x")) {
			move.setTakes(true);
		}
		int x = ((int) notation.charAt(notation.length() - 1)) - 49;
		int y = ((int) notation.charAt(notation.length() - 2)) - 97;
		move.setDestination(new Position(x, y));
		move.setNotation(notation);
		return move;
	}

	private Position isMoveValid(Move move, Piece[][] board) {
		Set<Position> possiblePositions = new HashSet<>();
		Position dest = move.getDestination();
		switch (move.getPieceToMove().getPieceType()) {
		case KING:
			for (int i = dest.getX() - 1; i <= dest.getX() + 1; i++) {
				for (int j = dest.getY() - 1; j <= dest.getY() + 1; j++) {
					possiblePositions.add(new Position(i, j));
				}
			}
			break;
		case QUEEN:
			possiblePositions = checkRank(dest, board, possiblePositions);
			possiblePositions = checkFile(dest, board, possiblePositions);
			possiblePositions = checkDiagonals(dest, board, possiblePositions);
			break;
		case BISHOP:
			possiblePositions = checkDiagonals(dest, board, possiblePositions);
			break;
		case ROOK:
			possiblePositions = checkRank(dest, board, possiblePositions);
			possiblePositions = checkFile(dest, board, possiblePositions);
			break;
		case KNIGHT:
			possiblePositions = isKnightPossible(dest.getX() + 2, dest.getY() + 1, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() + 2, dest.getY() - 1, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() + 1, dest.getY() + 2, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() + 1, dest.getY() - 2, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() - 2, dest.getY() + 1, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() - 2, dest.getY() - 1, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() - 1, dest.getY() + 2, board, possiblePositions);
			possiblePositions = isKnightPossible(dest.getX() - 1, dest.getY() - 2, board, possiblePositions);
			break;
		case PAWN:
			// if pawn has taken and is moved diagonal
			// NOTE: ignoring en-passant move for now
			if (move.isTakes()) {
				if (move.getPieceToMove().getPlayer().equals(Player.WHITE)) {
					possiblePositions = isPawnTakesPossible(dest.getX() - 1, dest.getY() - 1, possiblePositions);
					possiblePositions = isPawnTakesPossible(dest.getX() - 1, dest.getY() + 1, possiblePositions);
				} else {
					possiblePositions = isPawnTakesPossible(dest.getX() + 1, dest.getY() - 1, possiblePositions);
					possiblePositions = isPawnTakesPossible(dest.getX() + 1, dest.getY() + 1, possiblePositions);
				}
			} else {
				// normal one step move
				if (move.getPieceToMove().getPlayer().equals(Player.WHITE)) {
					possiblePositions.add(new Position(dest.getX() - 1, dest.getY()));
				} else {
					possiblePositions.add(new Position(dest.getX() + 1, dest.getY()));
				}
				// first 2 step move
				if (move.getPieceToMove().getPlayer().equals(Player.WHITE)) {
					possiblePositions.add(new Position(1, dest.getY()));
				} else {
					possiblePositions.add(new Position(6, dest.getY()));
				}
			}
			break;
		default:
			break;
		}
		log.info("all possible positions {}", possiblePositions);
		for (Position position : possiblePositions) {
			Piece piece = board[position.getX()][position.getY()];
			if (Objects.nonNull(piece) && piece.equals(move.getPieceToMove())) {
				return position;
			}
		}
		return null;
	}

	private Set<Position> isPawnTakesPossible(int x, int y, Set<Position> positions) {
		if (x >= 0 && x < 8 && y >= 0 && y < 8) {
			positions.add(new Position(x, y));
		}
		return positions;
	}

	private Set<Position> isKnightPossible(int x, int y, Piece[][] board, Set<Position> positions) {
		if (x >= 0 && x < 8 && y >= 0 && y < 8 && Objects.nonNull(board[x][y])) {
			positions.add(new Position(x, y));
		}
		return positions;
	}

	private Set<Position> checkFile(Position dest, Piece[][] board, Set<Position> positions) {
		for (int j = dest.getY() - 1; j >= 0; j--) {
			positions.add(new Position(dest.getX(), j));
			if (Objects.nonNull(board[dest.getX()][j])) {
				break;
			}
		}
		for (int j = dest.getY() + 1; j < 8; j++) {
			positions.add(new Position(dest.getX(), j));
			if (Objects.nonNull(board[dest.getX()][j])) {
				break;
			}
		}
		return positions;
	}

	private Set<Position> checkRank(Position dest, Piece[][] board, Set<Position> positions) {
		for (int i = dest.getX() - 1; i >= 0; i--) {
			positions.add(new Position(i, dest.getY()));
			if (Objects.nonNull(board[i][dest.getY()])) {
				break;
			}
		}
		for (int i = dest.getX() + 1; i < 8; i++) {
			positions.add(new Position(i, dest.getY()));
			if (Objects.nonNull(board[i][dest.getY()])) {
				break;
			}
		}
		return positions;
	}

	private Set<Position> checkDiagonals(Position dest, Piece[][] board, Set<Position> positions) {
		for (int i = dest.getX() - 1, j = dest.getY() - 1; i >= 0 && j >= 0; i--, j--) {
			positions.add(new Position(i, j));
			if (Objects.nonNull(board[i][j])) {
				break;
			}
		}
		for (int i = dest.getX() - 1, j = dest.getY() + 1; i >= 0 && j < 8; i--, j++) {
			positions.add(new Position(i, j));
			if (Objects.nonNull(board[i][j])) {
				break;
			}
		}
		for (int i = dest.getX() + 1, j = dest.getY() - 1; i < 8 && j >= 0; i++, j--) {
			positions.add(new Position(i, j));
			if (Objects.nonNull(board[i][j])) {
				break;
			}
		}
		for (int i = dest.getX() + 1, j = dest.getY() + 1; i < 8 && j < 8; i++, j++) {
			positions.add(new Position(i, j));
			if (Objects.nonNull(board[i][j])) {
				break;
			}
		}
		return positions;
	}

	@Override
	public ChessGame getGame(String token) {
		return repository.findById(token).orElse(null);
	}

}
