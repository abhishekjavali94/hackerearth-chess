package com.hackerearth.chess.web.model;

import java.util.List;

import com.hackerearth.chess.constant.Player;
import com.hackerearth.chess.entity.ChessGame.Move;
import com.hackerearth.chess.entity.ChessGame.Piece;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChessGameResponse {

	private String token;

	private boolean whiteToMove;

	private Player winner;

	private List<Move> moves;

	private Piece[][] board;

}
