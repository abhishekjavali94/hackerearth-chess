package com.hackerearth.chess.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackerearth.chess.entity.ChessGame;
import com.hackerearth.chess.service.MovesService;

@RestController
public class MovesController {

	@Autowired
	private MovesService movesService;

	@PostMapping(path = "/move")
	public String move(@RequestBody MoveRequest moveRequest) {
		return movesService.movePiece(moveRequest);
	}
	
	@GetMapping(path = "/game")
	public ChessGame getGame(@RequestParam(value = "token", required = true) String token) {
		return movesService.getGame(token);
	}

}
