package com.hackerearth.chess.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hackerearth.chess.entity.ChessGame;

public interface ChessGameRepository extends MongoRepository<ChessGame, String> {

}
