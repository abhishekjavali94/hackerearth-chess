package com.hackerearth.chess.util;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class Utility {

	/**
	   * Fill object with given source value using {@link BeanUtils}
	   *
	   * @param source      source object
	   * @param destination destination object
	   * @param <T>         source object type
	   * @param <R>         destination object type
	   * @return return destination object
	   */
	  public static <T, R> R shallowCopy(T source, R destination) {
	    BeanUtils.copyProperties(source, destination);
	    return destination;
	  }

	  /**
	   * Fill object with given source value using {@link BeanUtils}
	   *
	   * @param source      source object
	   * @param destination destination object
	   * @param ignored     ignored property fields
	   * @param <T>         source object type
	   * @param <R>         destination object type
	   * @return return destination object
	   */
	  public static <T, R> R shallowCopy(T source, R destination, String... ignored) {
	    BeanUtils.copyProperties(source, destination, ignored);
	    return destination;
	  }

	  /**
	   * Fill object (recursive deep copy) with given source value using {@link DozerBeanMapper}
	   *
	   * @param source      source object
	   * @param destination destination object
	   * @param <T>         source object type
	   * @param <R>         destination object type
	   * @return return destination object
	   */
	  public <T, R> R deepCopy(T source, R destination) {
	    new DozerBeanMapper().map(source, destination);
	    return destination;
	  }
}
