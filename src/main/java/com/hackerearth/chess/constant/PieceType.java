package com.hackerearth.chess.constant;

public enum PieceType {
	
	KING,
	QUEEN,
	BISHOP,
	KNIGHT,
	ROOK,
	PAWN;

}
