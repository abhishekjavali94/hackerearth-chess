package com.hackerearth.chess.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.hackerearth.chess.constant.PieceType;
import com.hackerearth.chess.constant.Player;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
public class ChessGame {

	@Id
	private String token;

	private boolean whiteToMove;

	private Player winner;

	private List<Move> moves;

	private Piece[][] board;

	public ChessGame() {
		this.whiteToMove = true;
		this.moves = new ArrayList<>();

		this.board = new Piece[8][8];
		this.board[0][0] = new Piece(Player.WHITE, PieceType.ROOK);
		this.board[0][1] = new Piece(Player.WHITE, PieceType.KNIGHT);
		this.board[0][2] = new Piece(Player.WHITE, PieceType.BISHOP);
		this.board[0][3] = new Piece(Player.WHITE, PieceType.QUEEN);
		this.board[0][4] = new Piece(Player.WHITE, PieceType.KING);
		this.board[0][5] = new Piece(Player.WHITE, PieceType.BISHOP);
		this.board[0][6] = new Piece(Player.WHITE, PieceType.KNIGHT);
		this.board[0][7] = new Piece(Player.WHITE, PieceType.ROOK);

		this.board[7][0] = new Piece(Player.BLACK, PieceType.ROOK);
		this.board[7][1] = new Piece(Player.BLACK, PieceType.KNIGHT);
		this.board[7][2] = new Piece(Player.BLACK, PieceType.BISHOP);
		this.board[7][3] = new Piece(Player.BLACK, PieceType.QUEEN);
		this.board[7][4] = new Piece(Player.BLACK, PieceType.KING);
		this.board[7][5] = new Piece(Player.BLACK, PieceType.BISHOP);
		this.board[7][6] = new Piece(Player.BLACK, PieceType.KNIGHT);
		this.board[7][7] = new Piece(Player.BLACK, PieceType.ROOK);

		for (int i = 0; i < 8; i++) {
			this.board[1][i] = new Piece(Player.WHITE, PieceType.PAWN);
			this.board[6][i] = new Piece(Player.BLACK, PieceType.PAWN);
		}
	}

	@Data
	@Builder
	@AllArgsConstructor
	public static class Move {
		
		private String notation;

		private Piece pieceToMove;

		private boolean takes;

		private Position destination;
		
		private String castlingType;
		
		public Move() {
			this.takes = false;
		}
		
		public Move(String castlingType, Player player) {
			this.pieceToMove = new Piece(player, PieceType.KING);
			this.takes = false;
			this.castlingType = castlingType;
		}
	}

	@Data
	@Builder
	@AllArgsConstructor
	public static class Piece {

		private Player player;

		private PieceType pieceType;
	}

	@Data
	@Builder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Position {

		int x;

		int y;

	}

}
